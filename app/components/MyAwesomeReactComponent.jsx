import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
//import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

const styles = {
  propContainer: {
    width: 200,
    overflow: 'hidden',
    margin: '20px auto 0',
  },
  propToggleHeader: {
    margin: '20px auto 10px',
  },
};


class TestTable extends React.Component {
  render() {
    return (
        <table>
          <tbody>
            <tr >
              <td>123</td>
              <td>asdad</td>
            </tr>
          </tbody>
        </table>
      )
  }
}

export default class AppBarExampleIcon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div>
        <AppBar
          title="Title"
          iconClassNameRight="muidocs-icon-navigation-expand-more"
        />
        {/*<RaisedButton label="Default" />*/}
        <TestTable />
      </div>
    );
  }
}