
## Simple React boilerplate ideal for Vagrant
---

**Steps to get started**

1. ** git clone  **
2. ** npm install **
3. ** npm start **

Your server should run on port **5010**

If you want to change the port, configure it in ** webpack.config.js **
