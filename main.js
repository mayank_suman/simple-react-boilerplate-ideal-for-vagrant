import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MyAwesomeReactComponent from './app/conponents/MyAwesomeReactComponent.jsx';

import App from './app/components/App.jsx';
//import Mood from './app/components/mood.jsx';

// const App = () => (
//   <MuiThemeProvider>
//     <MyAwesomeReactComponent />
//   </MuiThemeProvider>
// );

// ReactDOM.render(<Mood />, document.getElementById('mood-container'));
ReactDOM.render(<App />, document.getElementById('app'));