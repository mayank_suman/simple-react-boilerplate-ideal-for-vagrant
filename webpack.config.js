const path = require('path');

module.exports = {
	entry: './main.js',

	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'index.js'
	},

	devServer: {
		host: '0.0.0.0',
		inline: true,
		port: 5010,
		disableHostCheck: true
	},

	watch: true,

	module: {
		loaders: [{
			test: /\.jsx?$/,
			exclude: /node_modules/,
			loader: 'babel-loader',

			query: {
				presets: ['es2015', 'stage-0', 'react']
			}
		}]
	}
};
